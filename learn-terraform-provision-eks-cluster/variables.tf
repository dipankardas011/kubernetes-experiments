# Copyright (c) HashiCorp, Inc.
# SPDX-License-Identifier: MPL-2.0

variable "region" {
  description = "AWS region"
  type        = string
  default     = "ap-south-1"
}

variable "cluster_name" {
  description = "cluster name"
  type        = string
  default     = "demo-viamagus"
}

variable "cluster_version" {
  description = "k8s version"
  type        = string
  default     = "1.27"
}
