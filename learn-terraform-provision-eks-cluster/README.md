# Learn Terraform - Provision an EKS Cluster

This repo is a companion repo to the [Provision an EKS Cluster tutorial](https://developer.hashicorp.com/terraform/tutorials/kubernetes/eks), containing
Terraform configuration files to provision an EKS cluster on AWS.

# for polcies
AmazonEC2ContainerRegistryReadOnly
AmazonEKSClusterPolicy
AmazonEKSServicePolicy
AmazonEKSWorkerNodePolicy

and add the policy.json

## To add more user the access to kubernetes cluster

add this permissions

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "Statement1",
            "Effect": "Allow",
            "Action": [
                "eks:DescribeCluster",
                "eks:ListClusters"
            ],
            "Resource": "*"
        }
    ]
}
```

```bash
aws eks update-kubeconfig --region us-east-1 --name demo-viamagus --profile ***
```

```bash
# for the configureing the access to the kubeconfig
kubectl edit cm aws-auth -n kube-system
# and add the below
```

```yaml
data:
  ...
  mapUsers: |
    - userarn: arn:aws:iam::XXXXXX:user/YYY
      username: YYY
      groups:
      - system:masters # do check what all things are possible
```
