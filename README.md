# Steps for Gateway api
## Install
```bash
kubectl apply -f https://github.com/kubernetes-sigs/gateway-api/releases/download/v0.7.1/standard-install.yaml
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.12.0/cert-manager.yaml
git clone https://github.com/nginxinc/nginx-kubernetes-gateway.git
cd nginx-kubernetes-gateway
# also checkout to stable release branch
git checkout v0.4.0

kubectl apply -f deploy/manifests/namespace.yaml
kubectl create configmap njs-modules --from-file=internal/nginx/modules/src/httpmatches.js -n nginx-gateway

kubectl apply -f deploy/manifests/nginx-conf.yaml
kubectl apply -f deploy/manifests/rbac.yaml
kubectl apply -f deploy/manifests/gatewayclass.yaml
kubectl apply -f deploy/manifests/deployment.yaml

# for exposing
kubectl apply -f deploy/manifests/service/loadbalancer.yaml

# for aws
kubectl apply -f deploy/manifests/service/loadbalancer-aws-nlb.yaml
```

> add this flag to the cert-manager deployment configuration
```yml
- --feature-gates=ExperimentalGatewayAPISupport=true
```


```bash
cd gateway-experiment
k apply -f sample-deployment.yml
k apply -f gateway.yml
```

## DELETE
```bash
kubectl delete -f https://github.com/kubernetes-sigs/gateway-api/releases/download/v0.7.1/standard-install.yaml
kubectl delete -f https://github.com/cert-manager/cert-manager/releases/download/v1.12.0/cert-manager.yaml
git clone https://github.com/nginxinc/nginx-kubernetes-gateway.git
cd nginx-kubernetes-gateway
kubectl delete -f deploy/manifests/namespace.yaml
kubectl delete configmap njs-modules --from-file=internal/nginx/modules/src/httpmatches.js -n nginx-gateway

kubectl delete -f deploy/manifests/nginx-conf.yaml
kubectl delete -f deploy/manifests/rbac.yaml
kubectl delete -f deploy/manifests/gatewayclass.yaml
kubectl delete -f deploy/manifests/deployment.yaml

# for exposing
kubectl delete -f deploy/manifests/service/loadbalancer.yaml
```

## References
https://github.com/nginxinc/nginx-kubernetes-gateway/blob/main/docs/installation.md
https://gateway-api.sigs.k8s.io/guides/#installing-gateway-api

# Used the ingress (BACKUP)

install the nginx ingress controller

deploy the gateway-experiment/sample-deployment and gateway-experiment/ingress.yml


then configure your dns provider and add the cname or A record for the dns or the ipv4 address specific by your cloud provider

## Azure

```bash

# https://cert-manager.io/docs/installation/
kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.12.0/cert-manager.yaml

NAMESPACE=ingress-basic

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
helm repo update

helm install ingress-nginx ingress-nginx/ingress-nginx \
  --create-namespace \
  --namespace $NAMESPACE \
  --set controller.service.annotations."service\.beta\.kubernetes\.io/azure-load-balancer-health-probe-request-path"=/healthz
```

 > Deploy the ingress and the sample-deployment

then use the loadbalancer ip and add the A record

the use the domain you will be able to access

# NOTE

for the dns name to work you need to add the A record (if the loadbalancer IP is given Azure does it by default) ortherwise add CNAME record if dns name is given like AWS and CIVO does by default

# DISCLAMER
> currently GATEWAY tested on Azure
